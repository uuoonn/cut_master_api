package com.lyj.cutmasterapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.cdi.Eager;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Humanity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;

    @Column(nullable = false, length = 3)
    private String giveAndTake;

    @Column(nullable = false, length = 30)
    private String itemName;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false)
    private LocalDate gntToday;

}
