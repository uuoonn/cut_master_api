package com.lyj.cutmasterapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Friend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcText;

    @Column(nullable = false)
    private Boolean cutWhether;

    private LocalDate cutDate;

    @Column(length = 30)
    private String cutReasons;



}
