package com.lyj.cutmasterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CutMasterApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CutMasterApiApplication.class, args);
    }

}
