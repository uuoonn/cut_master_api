package com.lyj.cutmasterapi.repository;

import com.lyj.cutmasterapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend,Long> {
}
