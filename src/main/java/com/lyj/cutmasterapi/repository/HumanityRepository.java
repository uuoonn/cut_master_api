package com.lyj.cutmasterapi.repository;

import com.lyj.cutmasterapi.entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity,Long> {
}
