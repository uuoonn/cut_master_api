package com.lyj.cutmasterapi.model.Humanity;

import com.lyj.cutmasterapi.entity.Friend;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class HumanityItem {
    private Friend friendId;
    private String giveAndTake;
    private Double amount;
}
