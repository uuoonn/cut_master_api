package com.lyj.cutmasterapi.model.Humanity;

import com.lyj.cutmasterapi.entity.Friend;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class HumanityCreateRequest {
    private String giveAndTake;
    private String itemName;
    private Double amount;
    private LocalDate gntToday;
}
