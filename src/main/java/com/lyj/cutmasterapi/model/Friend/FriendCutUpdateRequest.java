package com.lyj.cutmasterapi.model.Friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class FriendCutUpdateRequest {
    private String cutReasons;

}
