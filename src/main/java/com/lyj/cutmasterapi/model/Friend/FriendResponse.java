package com.lyj.cutmasterapi.model.Friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class FriendResponse {
    private String name;
    private LocalDate birthDay;
    private String phoneNumber;
    private String etcText;
    private Boolean cutWhether;
    private LocalDate cutDate;
    private String cutReasons;
}
