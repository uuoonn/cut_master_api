package com.lyj.cutmasterapi.service;

import com.lyj.cutmasterapi.entity.Friend;
import com.lyj.cutmasterapi.model.Friend.*;
import com.lyj.cutmasterapi.repository.FriendRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id){
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request){
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcText(request.getEtcText());
        addData.setCutWhether(false);

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends(){
        List<Friend> originList = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();
        for(Friend friend :originList){
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setPhoneNumber(friend.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();
        FriendResponse response = new FriendResponse();
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcText(originData.getEtcText());
        response.setCutWhether(originData.getCutWhether());
        response.setCutReasons(originData.getCutReasons());
        response.setCutDate(originData.getCutDate());

        return response;


    }

    public void putFriend(long id, FriendChangeRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());

        friendRepository.save(originData);

    }

    public void putFriendCut(long id, FriendCutUpdateRequest request){
        Friend originData=friendRepository.findById(id).orElseThrow();
        originData.setCutWhether(true);
        originData.setCutDate(LocalDate.now());
        originData.setCutReasons(request.getCutReasons());

        friendRepository.save(originData);
    }

    public void putFriendCutCancel(long id){
        Friend originData=friendRepository.findById(id).orElseThrow();
        originData.setCutWhether(false);
        originData.setCutDate(null);
        originData.setCutReasons(null);

        friendRepository.save(originData);
    }


    //DEL 기능은 다중테이블을 엮는 기본 테이블에서는 사용하지않는다.


}
