package com.lyj.cutmasterapi.service;

import com.lyj.cutmasterapi.entity.Friend;
import com.lyj.cutmasterapi.entity.Humanity;
import com.lyj.cutmasterapi.model.Humanity.HumanityCreateRequest;
import com.lyj.cutmasterapi.model.Humanity.HumanityItem;
import com.lyj.cutmasterapi.repository.HumanityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHumanity(Friend friend,HumanityCreateRequest request){

        Humanity addData = new Humanity();
        addData.setId(friend.getId());
        addData.setGiveAndTake(request.getGiveAndTake());
        addData.setItemName(request.getItemName());
        addData.setAmount(request.getAmount());
        addData.setGntToday(request.getGntToday());

        humanityRepository.save(addData);
    }



}
