package com.lyj.cutmasterapi.controller;

import com.lyj.cutmasterapi.model.Friend.*;
import com.lyj.cutmasterapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/friend-create")
    private String setFriend(@RequestBody FriendCreateRequest request){
        friendService.setFriend(request);

        return "친구정보등록완료";
    }

    @GetMapping("/all")
    private List<FriendItem> getFriends(){
        return friendService.getFriends();
    }


    @GetMapping("/detail/{id}")
    private FriendResponse getFriend(@PathVariable long id){
        return friendService.getFriend(id);
    }

    @PutMapping("/change-friend-info/{id}")
    private String putFriend(@PathVariable long id , @RequestBody FriendChangeRequest request){
        friendService.putFriend(id,request);

        return "친구 정보 수정 완료";
    }

    @PutMapping("/cut-reason/{id}")
    public String putFriendCut(@PathVariable long id ,@RequestBody FriendCutUpdateRequest request){
        friendService.putFriendCut(id, request);

        return "친구 손절 이유 작성 완료";
    }


    @PutMapping("/cut-cancle-id/{id}")
    private String putFriendCutCancel(@PathVariable long id){
        friendService.putFriendCutCancel(id);

        return "친구 손절 취소 완료";
    }

}
