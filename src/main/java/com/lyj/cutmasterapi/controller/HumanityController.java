package com.lyj.cutmasterapi.controller;

import com.lyj.cutmasterapi.entity.Friend;
import com.lyj.cutmasterapi.model.Humanity.HumanityCreateRequest;
import com.lyj.cutmasterapi.service.FriendService;
import com.lyj.cutmasterapi.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/Humanity")

public class HumanityController {

    private final FriendService friendService;
    private final HumanityService humanityService;


    @PostMapping("/creat/friend-id/{friendId}")
    public String setHumanity(@PathVariable long friendId, @RequestBody HumanityCreateRequest request){
        Friend friend = friendService.getData(friendId);
        humanityService.setHumanity(friend,request);

        return "ok";
    }


}
